﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Net.NetworkInformation;
using System;


public class PUBGRequest : MonoBehaviour
{
    #region Singleton

    public static PUBGRequest _instance;

    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

    #endregion

    #region Private Member Variables

    private string url = "https://api.pubg.com/tournaments";
    private string key = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ZDRmZDM4MC1jNDYyLTAxMzgtM2I1OS0yOTY2M2Y3ZDk5YjIiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTk3ODUyNjI1LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImVzY2FtaWxsYWdvbnphIn0.XDJsdAzyBe0sksdWUg21j5cebk4N8FIs2hb7c5410vM";
    private string dataType = "application/vnd.api+json";

    private PUBGResponse _data;

    #endregion

    #region Public Methods

    public Datum[] GetTournaments()
    {
        return _data?.data.ToArray();
    }

    public void Request(Action callBack)
    {
        StartCoroutine(MakePUBGRequest(callBack));
    }

    #endregion

    #region Private Methods

    private IEnumerator MakePUBGRequest(Action callBack)
    {
        UnityWebRequest request = new UnityWebRequest(url);
        request.downloadHandler = new DownloadHandlerBuffer();
        
        request.SetRequestHeader("Authorization", key);
        request.SetRequestHeader("Accept", dataType);

        UnityWebRequest.Get(url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            _data = JsonConvert.DeserializeObject<PUBGResponse>(request.downloadHandler.text);
        }

        callBack?.Invoke();

    }
    #endregion
}
