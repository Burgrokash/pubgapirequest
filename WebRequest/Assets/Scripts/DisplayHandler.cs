﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayHandler : MonoBehaviour
{
    #region Serialized Fields

    /// <summary>
    /// Used to display the Tournament IDs.
    /// </summary>
    [SerializeField]
    private RectTransform leftLayout;

    /// <summary>
    /// Used to display the Tournament Upload Dates.
    /// </summary>
    [SerializeField]
    private RectTransform rightLayout;

    [SerializeField]
    private GameObject textPrefab;

    #endregion

    #region Public Methods

    /// <summary>
    /// Calls for the request on the PUBGRequest class while passing the CallBack.
    /// </summary>
    public void RequestData()
    {
        PUBGRequest._instance.Request(SetTexts);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Method used as callback when the WebRequest is finished.
    /// </summary>
    private void SetTexts()
    {
        Datum[] tournaments = PUBGRequest._instance.GetTournaments();
        string[] _data = new string[2];

        Debug.Log("Oli:" + PUBGRequest._instance.name);

        for (int i = 0; i < tournaments.Length; i++)
        {
            _data[0] = tournaments[i].id;
            _data[1] = tournaments[i].attributes.createdAt;

            SetNewTournamentText(_data);
        }
    }

    private void SetNewTournamentText(string[] textData)
    {
        Transform txtMeshTransform = GameObject.Instantiate(textPrefab).transform;
        TextMeshProUGUI txtMesh = txtMeshTransform.GetComponent<TextMeshProUGUI>();
        txtMeshTransform.SetParent(leftLayout);
        txtMesh.text = textData[0];
        txtMesh.rectTransform.localScale = Vector3.one;
        txtMesh.fontSize = 30;

        txtMeshTransform = GameObject.Instantiate(textPrefab).transform;
        txtMesh = txtMeshTransform.GetComponent<TextMeshProUGUI>();
        txtMeshTransform.SetParent(rightLayout);
        txtMesh.text = textData[1];
        txtMesh.rectTransform.localScale = Vector3.one;
        txtMesh.fontSize = 30;
    }
    #endregion
}
