﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attributes
{
    public string createdAt { get; set; }
}

public class Datum
{
    public string type { get; set; }
    public string id { get; set; }
    public Attributes attributes { get; set; }
}

public class Links
{
    public string self { get; set; }
}

public class Meta
{
}

public class PUBGResponse
{
    public List<Datum> data { get; set; }
    public Links links { get; set; }
    public Meta meta { get; set; }
}
